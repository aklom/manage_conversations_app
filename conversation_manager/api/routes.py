from flask import Blueprint, request, jsonify
from ..utils import Utils
from ..models import User
from conversation_manager import  sqldb, app
from recastai import Connect
from flask_wtf import  CSRFProtect
from flask_pymongo import PyMongo
from .functions import get_channel_slug, add_to_database

import requests, recastai, datetime, pusher, time
from .functions import  add_to_database

app.config["MONGO_URI"] = "mongodb://localhost:27017/db"

mongo = PyMongo(app)


apimod = Blueprint('api', __name__)

connect = Connect(Utils.REQ_TOKEN)

pusher_client = pusher.Pusher(
  app_id='574088',
  key='9b465c99e2e7e0b46bcb',
  secret='9398ee9d17b72adb45bf',
  cluster='eu',
  ssl=True
)


def reload_pages():

	pusher_client.trigger('inspect-channel', 'new-message', {'message': 'hello world'})
	pusher_client.trigger('index-channel', 'conversation-updated', {'message': 'hello world'})






@apimod.route('/update', methods= ['POST'])
def update():

	message = add_to_database(request)

	#conv = mongo.db.conversations.find_one({'id': message.conversation_id})
	#if conv['botId'] == session['botId']:
	reload_pages()

	return jsonify(status=200)

"""
@apimod.route('/fallback', methods= ['POST'])
def fallback(): 
	#### add status column to database
	# if unanswered -> status = PROBLEM and send notification
	
	message = connect.parse_message(request)
	conv_id = message.conversation_id
	
	
	#Adding or updating database
	if mongo.db.conversations.find_one(conv_id): 
		mongo.db.conversations.update_one({'id': conv_id}, {"$set": {'status': "FALLBACK"}})
		
	print("Conversation of id" + conv_id + " updated! ----> Fallback")
	
	
	return jsonify(status=200)
""" 


@apimod.route('/fallback', methods= ['POST'])
def fallback(): 
	#### add status column to database
	# if unanswered -> status = PROBLEM and send notification
	message = connect.parse_message(request)
	conv_id = message.conversation_id
	source = request.get_json()["nlp"]["source"]
	
	#updating database to add fallback status
	if mongo.db.conversations.find_one(conv_id): 
		mongo.db.conversations.update_one({'id': conv_id}, {"$set": {'status': "FALLBACK"}})
	
	print("Conversation of id" + conv_id + " updated! ----> Fallback")

	conv = mongo.db.conversations.find_one(conv_id)
	if conv["botmode"] == "ON": 
		if source != "FALLBACK":
			response = requests.post(Utils.CONV_URL + conv_id + '/messages', 
			json={'messages': [{'type': 'text', 'content': "Sorry, I don't understand."}]},
			headers={'Authorization': "Token {}".format(Utils.DEV_TOKEN)})
	
			print("Sorry message sent")
	else: 
		print("No answer ---> Bot mode is off ! ")
	
	
	return jsonify(status=200)

