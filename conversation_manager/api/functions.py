from flask import Blueprint, request, jsonify
from ..utils import Utils
from ..models import User
from conversation_manager import  sqldb, app
from recastai import Connect
from flask_wtf import  CSRFProtect
from flask_pymongo import PyMongo

import requests, recastai, datetime, pusher, time

app.config["MONGO_URI"] = "mongodb://localhost:27017/db"

mongo = PyMongo(app)

apimod = Blueprint('api', __name__)

connect = Connect(Utils.REQ_TOKEN)



def get_channel_slug(chid): 
	channels = requests.get(Utils.CHANNEL_URL, headers={'Authorization': "Token {}".format(Utils.DEV_TOKEN)}).json()
	#print(channels)
	channels = channels["results"]

	for channel in channels: 
		if channel["id"] == chid: 
			return channel["slug"]

	return "not-found"



def add_to_database(request): 
	#parse_message() is used to get the conversation id
	message = connect.parse_message(request)
	conv_id = message.conversation_id
	
	#See conversation memory
	#print(message.conversation)
	

	#Conversation content
	
	conversation = requests.get(Utils.CONV_URL + conv_id, headers={'Authorization': "Token {}".format(Utils.DEV_TOKEN)}).json()
	conversation = conversation["results"]

	print(conversation)
	
	channel = get_channel_slug(conversation["channel"])
	'''
	if "userName" in message.conversation["participant_data"]: 
		print(message.conversation)
		username = message.conversation["participant_data"]["userName"]
	else: 
		participants = conversation["participants"]
		for participant in participants: 
			if participant["isBot"] == False: 
				username = participant["id"]
	'''
	date = datetime.datetime.now().strftime("%I:%M%p on %B %d, %Y")
	
	memory = message.conversation['memory']
	username =""
	if "person" in memory: 
		username = memory['person']['fullname']
		print(username)
	email = ""
	if "email-address" in memory: 
		email = memory["email-address"]["raw"]
		print(email)

	phone = ""
	if "phone-number" in memory: 
		phone = memory["phone-number"]["value"]
		print(phone)
	#Adding or updating database
	if not mongo.db.conversations.find_one({"id": conv_id}): 
		print("Adding a new conversation")
		convv = {
			"id": conv_id, 
			"date": date, 
			"username": username, 
			"channel": channel, 
			"status": "OK", 
			"botmode": "ON", 
			"email": email, 
			"phone": phone
		}
		database_add_conv_to_users(conversation, conv_id)
		mongo.db.conversations.insert_one(convv)

	row_to_update = mongo.db.conversations.find_one({"id": conv_id})
	
	row_to_update["date"] = date

	if row_to_update["username"] == "":
		row_to_update["username"] = username
	if row_to_update["email"] == "":
		row_to_update["email"] = email
	if row_to_update["phone"] == "":
		row_to_update["phone"] = phone

	mongo.db.conversations.replace_one({'id': conv_id}, row_to_update)	

	print("Conversation of id " + conv_id + " updated!")

		
	return message




def database_add_conv_to_users(conversation, cid):

	connector = conversation["connector"] 

	connector_info = requests.get(Utils.CONNECTOR_URL + connector, headers={'Authorization': "Token {}".format(Utils.DEV_TOKEN)}).json()

	bot_id = connector_info["results"]["botId"]

	print(bot_id)
	users = mongo.db.users.find({'bot.botId': bot_id, 'owner': None })
	for user in users: 
		print(user['username']) 

	mongo.db.users.update({"bot.botId":bot_id} , {"$push": {'conversations': { 'id' : cid}}})






