from flask import session, Blueprint, jsonify, render_template, request, url_for, redirect, make_response
from ..utils import Utils
from recastai import Connect

from flask_pymongo import PyMongo
from ..models import User, LoginForm


from .functions import  get_conversation_content, delete_conversation_from_database, get_conversations_list
from .functions import get_groups_list, send_to_groups, send_to_all_users
from .functions import session_required, get_managers_list, create_manager_user
import recastai, requests, datetime
from datetime import datetime, timedelta
from conversation_manager import  sqldb, app

from flask_login import current_user
from werkzeug.security import generate_password_hash, check_password_hash

app.config["MONGO_URI"] = "mongodb://localhost:27017/db"

mongo = PyMongo(app)

sitemod = Blueprint('site', __name__, template_folder='templates', static_folder="static", static_url_path='/site/static')

curr_cid = ""

connect = Connect(Utils.REQ_TOKEN)


""" INBOX """

@sitemod.route('/inbox')
@session_required
def manage_conversations(current_user): 
	conv_list = get_conversations_list() 
	return render_template('index2.html', conversations=conv_list, owner=session['owner'])


""" INBOX """
@sitemod.route('/delete', methods=['POST'])
@session_required
def delete_conversation(current_user):
	#delete from database
	cid_del = request.form.get("conv_id")
	delete_conversation_from_database(cid_del)
	
	return redirect('/inbox')


""" INSPECT """

@sitemod.route('/inspect/<conversation_id>', methods=['POST', 'GET'])
@session_required
def inspect_conversation(current_user, conversation_id): 
	conversation = get_conversation_content(conversation_id)
	conv_params = mongo.db.conversations.find_one({'id': conversation_id}); 
	
	return render_template('inspect2.html', conversation=conversation, conversation_params= conv_params, owner=session['owner'])

	
""" INSPECT """

@sitemod.route('/send_message/<conversation_id>', methods=['POST', 'GET'])
@session_required
def send_message(current_user, conversation_id): 
	if request.method == 'POST': 
		
		message_to_send = request.form.get("message_to_send")
		
		response = requests.post(
			Utils.CONV_URL + conversation_id + '/messages', 
			json={'messages': [{'type': 'text', 'content': "{}".format(message_to_send)}]},
			headers={'Authorization': "Token {}".format(Utils.DEV_TOKEN)}
		)
		return redirect('/inspect/' + conversation_id)
	
	else: 
		return redirect('/inbox')


""" INSPECT """

@sitemod.route('/setbotmode/<conversation_id>', methods= ['POST', 'GET'])
@session_required
def setbotmode(conversation_id, current_user): 

	botmode = request.form.get("bot_mode")
	
	if(botmode == "ON"):
		params = {
			'message': {'type': 'text', 'content': 'FALLBACK'}, 
			'conversation_id': conversation_id, 
			'memory': {'bot-mode': 'on', }
		}
		mongo.db.conversations.update_one(
			{'id': conversation_id}, 
			{ "$set": { "botmode": "ON" } }
		)
		
	else: 
		params = {
			'message': {'type': 'text', 'content': 'FALLBACK'}, 
			'conversation_id': conversation_id, 
			'memory': {'bot-mode': 'off'}
		}
		mongo.db.conversations.update_one(
			{'id': conversation_id}, 
			{ "$set": { "botmode": "OFF" } }
		)

	response = requests.post(
		'https://api.recast.ai/build/v1/dialog', 
		json=params, 
		headers={'Authorization': "Token {}".format(Utils.DEV_TOKEN)})

	return redirect('/inspect/' + conversation_id)



""" GROUPS """

@sitemod.route('/groups', methods=['POST', 'GET'])
@session_required
def groups(current_user): 
	
	groups_list = get_groups_list()
	
	return render_template('groups.html', groups=groups_list, owner=session['owner'])



""" GROUPS """

@sitemod.route('/deletegroup', methods=['POST', 'GET'])
@session_required
def deletegroup(current_user): 
	
	mongo.db.users.update({"username": current_user}, {"$pull": {'groups':  {'id' : group_id}}})
	print("Group deleted! ")
	
	return redirect('/groups')



""" CREATE GROUP """

@sitemod.route('/creategroup', methods=['POST', 'GET'])
@session_required
def create_group(current_user): 
	
	conv_list = get_conversations_list()
	
	if request.method == 'POST': 
		""" Create the group """
		group = {
			"id": str(mongo.db.groups.count() + 1),
			"name": request.form.get("name"),
			"description": request.form.get("description"),
			"conversations": request.form.getlist("conv"),
			"username": current_user
		}
		mongo.db.groups.insert_one(group)

		return redirect('/groups')
	
	else: 
		""" display """ 
		return render_template('creategroup.html', conversations=conv_list, owner=session['owner'])


""" EDIT GROUP """

@sitemod.route('/editgroup', methods=['POST', 'GET'])
@session_required
def editgroup(current_user): 
	conv_list = get_conversations_list()

	group_id = request.form.get("group_id") 
	print("Editing group :" + group_id)

	group = mongo.db.groups.find_one({'id': group_id})

	if request.method == 'POST': 
		
		name = group["name"]
		desc = group["description"]
		participants = group["conversations"]

		mongo.db.groups.update_one({'id': group_id}, {'name': name, 'description': desc, 'conversations': participants})
		
		return render_template('editgroup.html', group_id=group_id, name=name, description=desc, participants=participants, conversations=conv_list, owner=session['owner'])


""" EDIT GROUP """

@sitemod.route('/savechanges', methods=['POST', 'GET'])
@session_required
def savechanges(current_user): 
	group2 = {
			"id": int(request.form.get("group_id")),
			"name": request.form.get("name"),
			"description": request.form.get("description"),
			"conversations": request.form.getlist("conv"), 
			"username": current_user
		}

	mongo.db.groups.replace_one({'id': int(request.form.get("group_id"))}, group2)
	
	return redirect('/groups')



""" BROADCAST """

@sitemod.route('/broadcast', methods=['POST', 'GET'])
@session_required
def broadcast(current_user): 
	if request.method == 'POST': 
		
		#message to send
		message = request.form.get("message")

		# groups selected
		groups = request.form.getlist("groups")
		
		if "all_users" in groups: 
			send_to_all_users(message)
		else: 
			send_to_groups(groups, message)

		return redirect ('/broadcast')
	else: 
		groups_list = []
		groups = mongo.db.groups.find({'username': current_user})

		for group in groups: 
			groups_list.append({'name': "{}".format(group["name"]), 'id': "{}".format(group["id"])})

		return render_template('broadcast.html', groups=groups_list, owner=session['owner'])



@sitemod.route('/testuser')
@session_required
def testuser(current_user): 
	print(current_user)
	return jsonify(status=200)


""" LOGIN """

@sitemod.route('/login', methods=['GET'])
def login(): 
	return render_template('login.html')


""" LOG IN """

@sitemod.route('/signin', methods=['POST'])
def signin():
	username = request.form.get("name")
	password = request.form.get("password")

	user = mongo.db.users.find_one({'username': "{}".format(username)})

	if not user: 
		return jsonify({'message': 'User not found'})

	account = user['account']
	print(account)
	if check_password_hash(account['password'], password): 
		session['username'] = username
		print(session)
		if user['owner'] == None: 
			session['owner'] = 1
		else: 
			session['owner'] = 0
		return redirect('/inbox')

	return jsonify({'message': 'Wrong password'})



""" LOG OUT """

@sitemod.route('/logout', methods=['POST'])
@session_required
def logout(current_user): 
	session.pop('username', None)
	session.pop('botId', None)
	return redirect('/login')


""" REGISTER """

@sitemod.route('/register')
def register(): 
	return render_template('register.html')

""" REGISTER """

@sitemod.route('/signup', methods=['POST'])
def signup(): 

	fname = request.form.get("first-name")
	lname = request.form.get("last-name")
	
	username = request.form.get("username")
	password = request.form.get("password")
	password2 = request.form.get("password2")

	botname = request.form.get("bot-name")
	req_token = request.form.get("req-token")
	dev_token = request.form.get("dev-token")

	users = mongo.db.users.find()

	for user in users: 
		if user['username'] == username: 
			return jsonify({'message': 'Username exists already'})

	if password2 != password: 
		return jsonify({'message': 'Wrong passwords'})

	hashed_password = generate_password_hash(password, method='sha256')

	account = {
		'password': hashed_password, 
		'first-name': fname,
		'last-name': lname
	}

	bot = {
		'bot-name': botname, 
		'dev-token': dev_token, 
		'req-token': req_token
	}

	user = {
		'username': username,
		'account': account,
		'bot': bot,
		'owner': None, 
		'conversations': []
	}

	mongo.db.users.insert_one(user)

	return redirect('/login')





@sitemod.route('/settings')
@session_required
def settings(current_user): 
	

	if session['owner'] == 1: 
		groups = get_groups_list()
		managers = get_managers_list()
		print(managers)
		return render_template('settings.html', groups=groups, managers=managers, owner=session['owner'])
	else: 
		return render_template('settings-manager.html', owner=session['owner'])

	
@sitemod.route('/addmanager', methods=['POST'])
@session_required
def addmanager(current_user): 

	username_manager = request.form.get("manager-name")
	print(username_manager)
	password_manager = request.form.get("manager-pwd")
	all_users = request.form.get("all")
	groups = request.form.getlist("groups")

	create_manager_user(username_manager, password_manager, all_users, groups)
	
	return redirect('/settings')