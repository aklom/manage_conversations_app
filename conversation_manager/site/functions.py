from flask import Blueprint, session, redirect, render_template, request, jsonify, session
from ..models import User
from ..utils import Utils
from conversation_manager import sqldb, app
from flask_pymongo import PyMongo
from datetime import datetime
import recastai, requests, jwt

from werkzeug.security import generate_password_hash, check_password_hash


from functools import wraps 

app.config["MONGO_URI"] = "mongodb://localhost:27017/db"

mongo = PyMongo(app)


""" Inbox + Edit group + Create group """
def get_conversations_list():
	
	current_user = session['username'] 
	user = mongo.db.users.find_one({'username': current_user})
	
	conversation_ids = user['conversations']
	
	conversations = []
	for conv_id in conversation_ids: 
		conv = mongo.db.conversations.find_one({'id': conv_id["id"]})
		conversations.append(conv)
	
	conversations.sort(key=lambda r: datetime.strptime(r['date'], "%I:%M%p on %B %d, %Y"), reverse = True)
	
	return conversations

""" Inbox """
def delete_conversation_from_database(cid_del): 

	current_user = session['username'] 
	mongo.db.users.update({"username": current_user}, {"$pull": {'conversations': { 'id' : cid_del}}})

	print("Conversation of id " + cid_del + " deleted!")
		



""" Groups """
def get_groups_list():

	current_user = session['username'] 
	groups = mongo.db.groups.find({'username': current_user})

	groups_list = []
	
	for group in groups:
		groups_list.append({
			'name': "{}".format(group["name"]),
			 'id': "{}".format(group["id"])
		})

	return groups_list


""" Inspect """
def get_conversation_content(cid): 
	conversation = []
	url = Utils.CONV_URL + cid
	if cid == "":
		print("cid missing!")
		return {}
	conv = requests.get(url, headers={'Authorization': "Token {}".format(Utils.DEV_TOKEN)}).json()
	if(conv["results"] != None): 
		participants = conv["results"]["participants"]

		for participant in participants:
			if(participant['isBot'] == False):
				human_id = participant['id']
			else: 
				bot_id = participant['id']

		messages = conv["results"]["messages"]
		for message in messages: 
			if message['participant'] == human_id: 
				conversation.append({'isBot': False, 'content': "{}".format(message['attachment']['content'])})
			elif message['participant'] == bot_id: 
				conversation.append({'isBot': True, 'content': "{}".format(message['attachment']['content'])})
	return conversation

'''
def get_memory_content(cid): 
	conversation = []
	url = Utils.CONV_URL + cid
	if cid == "":
		print("cid missing!")
		return {}
	conv = requests.get(url, headers={'Authorization': "Token {}".format(Utils.DEV_TOKEN)}).json()
	if(conv["results"] != None): 
		memory = conv["results"]
		print(memory)
	return {}
'''

""" Broadcast """
def send_to_all_users(message):

	current_user = session['username'] 
	user = mongo.db.users.find_one({'username': current_user})

	conversation_ids = user["conversations"]
	
	for conversation_id in conversation_ids: 
		response = requests.post(
			Utils.CONV_URL + conversation_id + '/messages', 
			json={'messages': [{'type': 'text', 'content': "{}".format(message)}]},
			headers={'Authorization': "Token {}".format(Utils.DEV_TOKEN)}
		)


def get_convs_from_groups(groups): 
	participants_set = set()
	
	for group in groups: 
		print(group)
		group_db = mongo.db.groups.find_one({'id': str(group)})
		print(group_db)
		participants = group_db["conversations"]
		for participant in participants:
			participants_set.add(participant)

	return participants_set

""" Broadcast """
def send_to_groups(groups, message): 

	participants_set =  get_convs_from_groups(groups)
	
	for participant in participants_set: 
		response = requests.post(
			Utils.CONV_URL + participant + '/messages', 
			json={'messages': [{'type': 'text', 'content': "{}".format(message)}]},
			headers={'Authorization': "Token {}".format(Utils.DEV_TOKEN)}
		)


def session_required(f): 
	@wraps(f)
	def decorated(*args, **kwargs): 
		if not 'username' in session:
			return redirect('/login')
		current_user = session['username']
		return f(current_user, *args, **kwargs)

	return decorated


def get_managers_list(): 

	current_user = session['username']
	user = mongo.db.users.find_one({"username": current_user})

	managers_list = []
	for manager in user['managers']: 
		managers_list.append(manager['username'])
	return managers_list


def create_manager_user(username, pwd, all, groups): 

	conv_ids = []
	current_user = session['username']
	user = mongo.db.users.find_one({"username": current_user})

	if all: 

		conv_ids = user["conversations"]

	else: 
		conv_set = get_convs_from_groups(groups)
		for cid in conv_set: 
			conv_ids.append({"id": cid})

	hashed_password = generate_password_hash(pwd, method='sha256')


	user = {
		'username': username, 
		'account': {'password': hashed_password}, 
		'bot': user['bot'], 
		'owner': current_user, 
		'conversations': conv_ids, 
		'groups': []
	}

	mongo.db.users.update({'username': current_user}, {'$push': {'managers': {'username': username}}})

	mongo.db.users.insert_one(user)



