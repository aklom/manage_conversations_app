from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bootstrap import Bootstrap 
from flask_wtf import FlaskForm, CSRFProtect
from flask_login import LoginManager


app = Flask(__name__)
app.config['WTF_CSRF_CHECK_DEFAULT']= False

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///users.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

sqldb = SQLAlchemy(app)
login = LoginManager(app)

from conversation_manager.api.routes import apimod 
from conversation_manager.site.routes import sitemod

app.register_blueprint(site.routes.sitemod)
app.register_blueprint(api.routes.apimod, url_prefix="/api")

bootstrap = Bootstrap(app)

app.config['SECRET_KEY'] = "HelloHello"
csrf = CSRFProtect(app)
csrf.exempt("api.routes")

