from conversation_manager import app, sqldb, login
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField
from wtforms.validators import DataRequired, Email, Length


class User(sqldb.Model):
	
	id = sqldb.Column(sqldb.Integer, primary_key=True)
	public_id = sqldb.Column(sqldb.String(50), unique = True)
	username = sqldb.Column(sqldb.String(50))
	password = sqldb.Column(sqldb.String(80))
	owner = sqldb.Column(sqldb.Boolean)
	first_name = sqldb.Column(sqldb.String(50))
	last_name = sqldb.Column(sqldb.String(50))
	
 
	
class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email(), Length(min=6, max=40)])
    password = PasswordField('Password', validators=[DataRequired()])
